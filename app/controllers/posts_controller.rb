class PostsController < ApplicationController
    
    before_action :find_post, only:[:show,:edit,:update,:destroy]
    before_action :authenticate_user!, except:[:index,:show]

    def index
        @posts = Post.all.order("created_at DESC")
    end 

    def show 
        
    end 

    def edit
        @categories = Category.all.map{ |c| [c.name, c.id] }
    end

    def update
        @post.category_id = params[:category_id]
        if @post.update(post_params)
            redirect_to post_path
        else    
            render 'edit'
        end    
    end

    def destroy
        @post.destroy
        redirect_to root_path
    end  

    def create
        @post = current_user.posts.build(post_params)
        @categories = Category.all.map{ |c| [c.name, c.id] }
        if @post.save
            redirect_to root_path
        else
            render 'new'
        end       
    end

    def  new
        @post = current_user.posts.build
        @categories = Category.all.map{ |c| [c.name, c.id] }
    end    
    private
        def post_params
            params.require(:post).permit(:title,:message,:category_id)
        end    
        def find_post
            @post = Post.find(params[:id])
        end  
end
