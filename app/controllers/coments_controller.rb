class ComentsController < ApplicationController
    before_action :find_post
    before_action :find_coment, only: [:edit, :update,:destroy]
    before_action :authenticate_user!

    def create

        @coment= @post.coments.create(coment_params)
        @coment.user_id = current_user.id

        if@coment.save
            redirect_to post_path(@post)
        else 
            render 'new'
        end
    end    
    def edit
        @coment = @post.coments.find(params[:id])
    end    

    def update
        if @coment.update(coment_params)
            redirect_to post_path(@post)
        else
            render 'edit'
        end        
    end    

    def destroy 
        @coment.destroy
        redirect_to post_path(@post)
    end    

    private

        def coment_params
            params.require(:coment).permit(:content)
        end 
        def find_post
            @post = Post.find(params[:post_id])
        end
        def find_coment
            @coment = @post.coments.find(params[:id])
        end
end    
